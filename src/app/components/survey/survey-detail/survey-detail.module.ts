import { SurveyDetailComponent } from './survey-detail.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
    {
        path: '',
        component: SurveyDetailComponent
    }
];



@NgModule({
    imports: [ RouterModule.forChild( routes ) ],
    exports: []
})
export class SurveyDetailModule {}
